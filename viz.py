import numpy as np
import matplotlib.pyplot as plt


def draw_for_zoom_level(filename, zoom_level):
    xx = []
    yy = []

    for line in open(filename):
        x, y, z, w, l = line.rstrip('\n').split(',')

        if int(l) <= zoom_level:
            xx.append(float(x))
            yy.append(float(y))

    plt.scatter(xx, yy, s = 2.0, alpha = 1.0)


def zoom_views(filename):

    plt.figure(1, figsize=(12, 12))

    #plt.subplot(2,2,1)
    #draw_for_zoom_level(filename, 3)

    plt.subplot(2,2,2)
    draw_for_zoom_level(filename, 2)

    plt.subplot(2,2,3)
    draw_for_zoom_level(filename, 1)

    plt.subplot(2,2,4)
    draw_for_zoom_level(filename, 0)

    plt.show()


def single_view(filename):
    xx = []
    yy = []

    for line in open(filename):
        x, y = line.rstrip('\n').split(',')
        xx.append(float(x))
        yy.append(float(y))

    plt.scatter(xx, yy, s = 2.0, alpha = 1.0)
    plt.show()


if __name__ == "__main__":
    zoom_views('test_out2.csv')
    #single_view('test_out1.csv')

