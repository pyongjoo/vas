import os
import time
from subprocess import call

DATA_DIR = os.path.join(os.environ['data'], 'pas')
os.environ['LD_LIBRARY_PATH'] = "/home/pyongjoo/local/lib"


def runtimeByN():

    for suffix in ['10M', '1M', '100K', '10K']:
        inputFileName = os.path.join(DATA_DIR, 'original/g%s.csv' % suffix)
        outputFileName = os.path.join(DATA_DIR, 'sampled/g%s_1K.csv' % suffix)

        start_time = time.time()
        call(['./single', inputFileName, outputFileName,
              '2', '1', '1000', '1', '1.0', '-5', '5', '-5', '5'])
        elapsed_time = time.time() - start_time

        print "Elapsed Time: %f" % elapsed_time
        print
        print


def runtimeByK():

    for sample_size in [10, 100, 1000, 10000]:
        inputFileName = os.path.join(DATA_DIR, 'original/g1M.csv')
        outputFileName = os.path.join(DATA_DIR, 'sampled/g1M_%d.csv' % sample_size)

        start_time = time.time()
        call(['./single', inputFileName, outputFileName,
              '2', '1', str(sample_size), '1', '1.0', '-5', '5', '-5', '5'])
        elapsed_time = time.time() - start_time

        print "Elapsed Time: %f" % elapsed_time
        print
        print



if __name__ == "__main__":
    #runtimeByK()
    #runtimeByN()

