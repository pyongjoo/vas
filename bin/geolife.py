import os
import time
from subprocess import call

DATA_DIR = os.path.join(os.environ['data'], 'pas')
os.environ['LD_LIBRARY_PATH'] = "/home/pyongjoo/local/lib"



def sampleByK():

    for sample_size in [100, 1000, 10000, 100000]:
        inputFileName = os.path.join(DATA_DIR, 'original/geolife.csv')
        outputFileName = os.path.join(DATA_DIR, 'sampled/geolife_%d.csv' % sample_size)

        start_time = time.time()
        call(['./single', inputFileName, outputFileName,
              '2', '1', str(sample_size), '5', '0.2', '0', '50', '90', '130'])
        elapsed_time = time.time() - start_time

        print "Elapsed Time: %f" % elapsed_time
        print
        print



if __name__ == "__main__":
    sampleByK()

