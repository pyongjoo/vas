//
//  SingleStage.cpp
//  pas
//
//  Created by Yongjoo Park on 11/29/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include "DataSource.h"
#include "RandomSampling.h"
#include "SingleStage.h"
#include "Slow2SingleStage.h"
#include <cfloat>
#include <cstdlib>
#include <ctime>
#include <ctgmath>
#include <sstream>
#include <vector>


using namespace std;



void Slow2SingleStageSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup) {

    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    float r = recommendedR(xmin, xmax, ymin, ymax, k);;

    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "alpha: " << alpha << ", beta: " << beta << ", sample size: " << k << endl;
    cout << "sweep num: " << sweep_num << ", sample prob: " << sample_prob << ", r: " << r << endl;
    cout << "range: " << xmin << " " << xmax << " " << ymin << " " << ymax << endl;

    // Sampling
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    PASSampler sampler(r, alpha, beta, k);

    if (warmup) {
        cout << "warm sampler" << endl;
        sampler.warm(fileSource);
    }
    
    cout << "actual routine starts" << endl;
    for (int i = 0; i < sweep_num; i++) {
        DataPoint* point;

        // single sweep
        long counter = 0;

        while ((point = fileSource.readNext())) {
            float rn = ((float) rand()) / RAND_MAX;
            if (rn <= sample_prob) {
                sampler.check(*point);
            }
            delete point;

            if (++counter % 1000 == 0) {
                cout << "\r  counter: " << counter << flush;
            }
        }

        fileSource.rewind();

        cout << "; Sweep " << i << " Done." << endl;
    }
    
    
    // Output
    ofstream ofs;
    ofs.open(outputFileName);
    
    vector<DataPoint> sampled = sampler.getSampled();
    
    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << ",0" << endl;
    }
    
    ofs.close();

    cout << "Done." << endl << endl;
}



#pragma mark PASSampler

Slow2PASSampler::Slow2PASSampler(float r, float alpha, float beta, int k)
: r(r), alpha(alpha), beta(beta), k(k)
{ }


void Slow2PASSampler::warm(DataSource& source) {

    int strata_num = k;
    StratifiedSampler sampler(strata_num, k);

    sampler.sample(source);

    vector<DataPoint> sampled = sampler.getSampled();

    for (vector<DataPoint>::const_iterator it = sampled.begin();
            it != sampled.end(); it++) {
        check(*it);
    }

    source.rewind();
}


void Slow2PASSampler::check(const DataPoint& point)
{
    if (queue.uid_map.count(point.uid) > 0)
        return;
    
    
    if (queue._dataArray.size() < k) {
        Expand(point);
    }
    else {
        Replace(point);
    }
}


double Slow2PASSampler::resp_reduction(const vector<RespPoint>& rpoints, const DataPoint& newPoint, const int idx) {

    double existing_resp = 0;
    const DataPoint& old_point = rpoints[idx].point;

    for (int i = 0; i < rpoints.size(); i++) {
        if (i == idx) continue;

        const DataPoint& point = rpoints[i].point;
        existing_resp += circleIntersect(point, old_point, r);
    }


    double new_resp = 0;

    for (int i = 0; i < rpoints.size(); i++) {
        if (i == idx) continue;

        const DataPoint& point = rpoints[i].point;
        new_resp += circleIntersect(point, newPoint, r);
    }

    return existing_resp - new_resp;
}


void Slow2PASSampler::Expand(const DataPoint& point) {

    RespPoint rp(point, 0);

    //    put into queue for ordering
    queue._dataArray.push_back(rp);
}


// param not used
void Slow2PASSampler::Replace(const DataPoint& point) {

    int arraySize = queue._dataArray.size();
    double max_resp_reduction = -1;
    int best_idx = -1;

    for (int i = 0; i < arraySize; i++) {
        double resp_red = resp_reduction(queue._dataArray, point, i);

        if (resp_red > max_resp_reduction) {
            max_resp_reduction = resp_red;
            best_idx = i;
        }
    }

    if (best_idx >= 0) {
        RespPoint rp(point, 0);
        queue._dataArray[best_idx] = rp;
    }
}


vector<DataPoint> Slow2PASSampler::getSampled()
{
    vector<DataPoint> sampled;

    for (vector<RespPoint>::const_iterator it = queue._dataArray.begin();
            it != queue._dataArray.end(); it++) {
        sampled.push_back(it->point);
    }

    return sampled;
}


Slow2PASSampler::~Slow2PASSampler()
{ }


