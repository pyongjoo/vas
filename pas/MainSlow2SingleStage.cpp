#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include "Slow2SingleStage.h"


using namespace std;


// Usage:
// ./single inputFileName outputFileName \
//          alpha beta sampleSize sweepNum sampleProb xmin xmax ymin ymax


int main(int argc, const char * argv[])
{
    assert(argc == 12 || argc == 13);

    string inputFileName(argv[1]);
    string outputFileName(argv[2]);
    float alpha       = (float) strtod(argv[3], (char **) NULL);
    float beta        = (float) strtod(argv[4], (char **) NULL);
    int sample_size   = (int) strtol(argv[5], (char **) NULL, 10);
    int sweep_num     = (int) strtol(argv[6], (char **) NULL, 10);
    float sample_prob = (float) strtod(argv[7], (char **) NULL);

    float xmin = (float) strtod(argv[8], (char **) NULL);
    float xmax = (float) strtod(argv[9], (char **) NULL);
    float ymin = (float) strtod(argv[10], (char **) NULL);
    float ymax = (float) strtod(argv[11], (char **) NULL);

    vector<float> range;
    range.push_back(xmin);
    range.push_back(xmax);
    range.push_back(ymin);
    range.push_back(ymax);

    bool warmup = false;
    if (argc == 13) {
        int warmup_flag = (int) strtol(argv[12], (char **) NULL, 10);
        if (warmup_flag != 0) warmup = true;
    }


    Slow2SingleStageSampleWrapper(inputFileName, outputFileName,
        alpha, beta, sample_size, range, sweep_num, sample_prob, warmup);
    
    return 0;
}

