//
//  DataSource.h
//  pas
//
//  Created by Yongjoo Park on 11/27/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//
//
//
//

#ifndef __pas__DataSource__
#define __pas__DataSource__

#include <fstream>
#include <iostream>
#include <string>
#include <vector>


typedef unsigned long data_id_t;


class DataPoint {
    
public:
    
    float x;    // x coordinate
    float y;    // y coordinate
    float z;    // z coordinate (do not affect sampling)
    float w;    // data importance
    
    data_id_t uid;   // just id. assigned when this record is read by FileDataSource
    
    // zoom level is defined separately in a database.
};


// DataSource and its descentdants provide sequential acccess to data.
class DataSource {
    
public:
    
    const float xmin;
    const float xmax;
    const float ymin;
    const float ymax;

    
public:
    
    DataSource(const float xmin, const float xmax, const float ymin, const float ymax);
    
    // Pure virtual
    // the caller is responsible for the destruction of what's returned
    virtual DataPoint* readNext() = 0;
    
    virtual void rewind() = 0;
};


// Provide sequential access to data stored in a file.
// Input File Format:
//   each line should contain
//      x,y,z,w      OR
//      x,y,z
//   if the last field does not exist, the value is assumed to be 1. as you see,
//   the values are comma separated. all data are read as float, not double.
class FileDataSource : public DataSource {
    
private:
    
    std::ifstream* ifs;
    
    data_id_t _lineNumber;   // used to assign uid to records
    
    
public:
    
    FileDataSource(const std::string filename,
                   const float xmin, const float xmax, const float ymin, const float ymax);
    
    ~FileDataSource();
    
    
    virtual DataPoint* readNext();
    
    virtual void rewind();
    
    
private:
    
    DataPoint* parseLine(std::string& line);
};


// May not be used. Refer to DataPointStorage instead.
class MemoryDataSource : public DataSource {
    
private:
    
    std::vector<DataPoint> _dataArray;
    
    data_id_t _lineNumber;   // to remember the position to return
    
    
public:
    
    MemoryDataSource(float xmin, float xmax, float ymin, float ymax);
    
    void insertData(DataPoint* point);
    
    
    virtual DataPoint* readNext();
    
    virtual void rewind();
    
};



#endif /* defined(__pas__DataSource__) */

