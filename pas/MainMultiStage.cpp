#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include "MultiStage.h"


using namespace std;


// Usage:
// ./multi inputFileName outputFileName \
//          alpha beta zoomLevels sampleSize sweepNum sampleProb xmin xmax ymin ymax


int main(int argc, const char * argv[])
{
    assert(argc == 13);

    string inputFileName(argv[1]);
    string outputFileName(argv[2]);
    float alpha       = (float) strtod(argv[3], (char **) NULL);
    float beta        = (float) strtod(argv[4], (char **) NULL);
    int zoom_levels   = (int) strtol(argv[5], (char **) NULL, 10);
    int sample_size   = (int) strtol(argv[6], (char **) NULL, 10);
    int sweep_num     = (int) strtol(argv[7], (char **) NULL, 10);
    float sample_prob = (float) strtod(argv[8], (char **) NULL);

    float xmin = (float) strtod(argv[9], (char **) NULL);
    float xmax = (float) strtod(argv[10], (char **) NULL);
    float ymin = (float) strtod(argv[11], (char **) NULL);
    float ymax = (float) strtod(argv[12], (char **) NULL);

    vector<float> range;
    range.push_back(xmin);
    range.push_back(xmax);
    range.push_back(ymin);
    range.push_back(ymax);

    MultiStageSampleWrapper(inputFileName, outputFileName,
                            alpha, beta, sample_size, range, zoom_levels, sweep_num, sample_prob);
    
    return 0;
}

