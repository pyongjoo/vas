//
//  RandomSampling.cpp
//  pas
//
//  Created by Yongjoo Park on 12/3/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include "RandomSampling.h"
#include "DataSource.h"
#include "MultiStage.h"
#include <cassert>
#include <cfloat>
#include <ctgmath>
#include <string>
#include <vector>


using namespace std;


#pragma mark RandomSampler

void RandomSamplerWrapper(const string inputFileName,
                          const string outputFileName,
                          const int k)
{
    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "sample size: " << k << endl;
    
    // Sampling
    FileDataSource fileSource(inputFileName, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX);
    RandomSampler sampler(k);
    
    DataPoint* point;
    long counter = 0;
    
    while ((point = fileSource.readNext())) {
        sampler.check(*point);
        delete point;
        
        if (++counter % 1000 == 0) {
            cout << "\r  counter: " << counter << flush;
        }
    }
    
    // Output
    ofstream ofs;
    ofs.open(outputFileName);
    
    vector<DataPoint> sampled = sampler.getSampled();
    
    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << ",0" << endl;
    }
    
    ofs.close();
    
    cout << endl << "Done." << endl << endl;
}



RandomSampler::RandomSampler(const int k)
: k(k), checked_num(0)
{
    
}


void RandomSampler::check(DataPoint& point)
{
    if (dataArray.size() < k) {
        dataArray.push_back(point);
    }
    else {
        int n = 1;
        int m = checked_num + 1;
        
        int ri = n + rand() % int(m - n + 1);
        
        if (ri <= k) {
            dataArray[ri - 1] = point;
        }
    }
    
    checked_num++;
}


vector<DataPoint> RandomSampler::getSampled()
{
    return dataArray;
}




#pragma mark StratifiedSampler


void StratifiedSamplerWrapper(const std::string inputFileName,
                              const std::string outputFileName,
                              const int k, const int strata_num, const vector<float>& range)
{
    assert(range.size() == 4);
    
    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    assert(xmin < xmax);
    assert(ymin < ymax);
    
    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "sample size: " << k << endl;
    
    // Sampling
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    StratifiedSampler sampler(strata_num, k);
    
    sampler.sample(fileSource);
    
    
    // Output
    ofstream ofs;
    ofs.open(outputFileName);
    
    vector<DataPoint> sampled = sampler.getSampled();
    
    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << ",0" << endl;
    }
    
    ofs.close();
}


StratifiedSampler::StratifiedSampler(const int strata_num, const int k)
: strata_num(strata_num), k(k)
{
    
}

int_coordi_t StratifiedSampler::hash(const DataPoint& point, const vector<float>& range)
{
    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    int cell_num = int(sqrt(strata_num));
    float width = (xmax - xmin) / cell_num;
    float height = (ymax - ymin) / cell_num;
    
    int x_coordi = floor((point.x - xmin) / width);
    x_coordi = min(x_coordi, cell_num - 1);
    
    int y_coordi = floor((point.y - ymin) / height);
    y_coordi = min(y_coordi, cell_num - 1);
    
    return make_pair(x_coordi, y_coordi);
}


vector<vector<int> > StratifiedSampler::plan(DataSource& source, vector<float>& range)
{
    int cell_num = int(sqrt(strata_num));
    
    vector<vector<int> > countMatrix(cell_num, vector<int>(cell_num, 0));
    vector<vector<int> > assignMatrix(cell_num, vector<int>(cell_num, 0));
    
    // Count
    DataPoint* point;
    long counter = 0;
    
    while ((point = source.readNext())) {
        int_coordi_t coordi = hash(*point, range);
        
        countMatrix[coordi.first][coordi.second] += 1;
        
        delete point;
        
        if (++counter % 1000 == 0) {
            cout << "\r    counter: " << counter << flush;
        }
    }
    cout << endl;
    source.rewind();
    
    
    // Assign
    int assigned_num = 0;
    int i = 0, j = 0;
    
    while (assigned_num < k) {
        
        if (countMatrix[i][j] > 0) {
            assignMatrix[i][j] += 1;
            countMatrix[i][j] -= 1;
            assigned_num++;
        }
        
        // index increment
        j++;
        if (j >= cell_num) {
            i++; j = 0;
            if (i >= cell_num) {
                i = 0;
            }
        }
    }
    
    return assignMatrix;
}


void StratifiedSampler::sample(DataSource& source)
{
    vector<float> range;
    range.push_back(source.xmin);
    range.push_back(source.xmax);
    range.push_back(source.ymin);
    range.push_back(source.ymax);
    
    int cell_num = int(sqrt(strata_num));
    
    // Prepare uniform random sampler for each cell
    vector<vector<int> > assignMatrix = plan(source, range);
    
    vector<vector<RandomSampler*> > samplerMatrix;
    
    for (int i = 0; i < cell_num; i++) {
        vector<RandomSampler*> samplerArray;
        
        for (int j = 0; j < cell_num; j++) {
            RandomSampler* a_sampler = new RandomSampler(assignMatrix[i][j]);
            samplerArray.push_back(a_sampler);
        }

        samplerMatrix.push_back(samplerArray);
    }
    
    
    // Do Sample
    DataPoint* point;
    long counter = 0;
    
    while ((point = source.readNext())) {
        int_coordi_t coordi = hash(*point, range);
        
        samplerMatrix[coordi.first][coordi.second]->check(*point);
        
        delete point;
        
        if (++counter % 1000 == 0) {
            cout << "\r    counter: " << counter << flush;
        }
    }
    
    
    // collect the randomly sampled.
    for (int i = 0; i < cell_num; i++) {
        for (int j = 0; j < cell_num; j++) {
            vector<DataPoint> results = samplerMatrix[i][j]->getSampled();
            
            for (vector<DataPoint>::const_iterator it = results.begin();
                 it != results.end(); it++) {
                dataArray.push_back(*it);
            }
        }
    }
    
    
    // clear samplerMatrix
    for (int i = 0; i < cell_num; i++) {
        for (int j = 0; j < cell_num; j++) {
            delete samplerMatrix[i][j];
        }
    }
    
    cout << endl << "Done." << endl << endl;

}


vector<DataPoint> StratifiedSampler::getSampled()
{
    return dataArray;
}


