#include <cassert>
#include <iostream>
#include <string>
#include "RandomSampling.h"


using namespace std;


// Usage:
// ./single inputFileName outputFileName \
//          alpha beta sampleSize sweepNum sampleProb xmin xmax ymin ymax


int main(int argc, const char * argv[])
{
    assert(argc == 4);

    string inputFileName(argv[1]);
    string outputFileName(argv[2]);
    int sample_size   = (int) strtol(argv[3], (char **) NULL, 10);

    RandomSamplerWrapper(inputFileName, outputFileName, sample_size);

    return 0;
}

