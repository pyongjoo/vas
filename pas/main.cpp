//
//  main.cpp
//  pas
//
//  Created by Yongjoo Park on 11/27/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include <iostream>
#include <string>
#include <vector>

#include "DataSource.h"
#include "SingleStage.h"
#include "MultiStage.h"


using namespace std;


// Test of single-pass algorithm
void test1() {
    string filename = "/home/pyongjoo/workspace/PAS/test.csv";
    
    float xmin = -5;
    float xmax = 5;
    float ymin = -5;
    float ymax = 5;
    
    FileDataSource fileSource(filename, xmin, xmax, ymin, ymax);
    
    float r = 0.5;
    float alpha = 2.0;
    float beta = 1.0;
    int sample_size = 100;
    PASSampler sampler(r, alpha, beta, sample_size);
    
    for (int i = 0; i < 10; i++) {
        DataPoint* point;

        while ((point = fileSource.readNext())) {
            sampler.check(*point);
            delete point;
        }

        fileSource.rewind();

        cout << "Sweep " << i << " Done." << endl;
    }
    
    
    string outfile = "/home/pyongjoo/workspace/PAS/test_out1.csv";
    ofstream ofs;
    ofs.open(outfile);
    
    vector<DataPoint> sampled = sampler.getSampled();
    
    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << endl;
    }
    
    ofs.close();
}


void test2() {

    string inputFileName = "/Users/yongjoo/workspace/PAS/test.csv";
    string outputFileName = "/Users/yongjoo/workspace/PAS/test_out2.csv";

    // Parameters
    float alpha = 2.0;
    float beta = 1.0;
    int sample_size = 100;      // k

    vector<float> range;
    range.push_back(-5);        // xmin
    range.push_back(5);         // xmax
    range.push_back(-5);        // ymin
    range.push_back(5);         // ymax

    int zoom_levels = 3;
    int sweep_num = 5;
    float sample_prob = 1;


    MultiStageSampleWrapper(inputFileName, outputFileName,
                             alpha, beta, sample_size, range, zoom_levels, sweep_num, sample_prob);
}



int main(int argc, const char * argv[])
{
    test1();
    
    return 0;
}

