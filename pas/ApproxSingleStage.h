
#ifndef __pas__ApproxSingleStage__
#define __pas__ApproxSingleStage__

#include "DataSource.h"
#include "SingleStage.h"
#include <iostream>
#include <spatialindex/SpatialIndex.h>
#include <spatialindex/Point.h>
#include <spatialindex/capi/IdVisitor.h>
#include <unordered_map>
#include <vector>


using namespace SpatialIndex;


class DataPointWithNeighbor {

public:

    DataPoint point;
    std::unordered_map<data_id_t, int> neighbors;         // responsibility in a sample

public:

    DataPointWithNeighbor();        // this should not be called

    DataPointWithNeighbor(DataPoint point, std::vector<data_id_t> neighbors);
};


class ApproxPASSampler : public PASSampler {

protected:

    std::unordered_map<data_id_t, DataPointWithNeighbor>* points_map;
    
public:
    
    /**
     * @param alpha weights for individual items (set 0 for defaults)
     * @param beta weights for pairwise interaction (set 1 for defaults)
     */
    ApproxPASSampler(float r, float alpha, float beta, int k);

    ~ApproxPASSampler();

    virtual void warm(DataSource& source);
    
    // Check for valid replacement.
    virtual void check(const DataPoint& point);
    
    virtual void Expand(const DataPoint& point);
    
    virtual void ExpandAndShrink(const DataPoint& point);

    virtual std::vector<DataPoint> getSampled();
};


void ApproxSingleStageSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup);


#endif /* defined(__pas__ApproxSingleStage__) */
