//
//  MultiStage.h
//  pas
//
//  Created by Yongjoo Park on 11/29/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#ifndef __pas__MultiStage__
#define __pas__MultiStage__

#include <iostream>
#include <utility>
#include <unordered_map>
#include <vector>
#include "DataSource.h"


typedef std::pair<int, int> int_coordi_t;


class Partitioner {
    
private:
    
    // assigned upon creation
    const float xmin;
    const float xmax;
    const float ymin;
    const float ymax;
    
    // zoom_level = 0 means 'no zoom' or equvalently 'overview'.
    // zoom_level = 1 means an one-level zoom-in.
    const int zoom_level;
    
public:
    
    Partitioner(const float xmin, const float xmax, const float ymin, const float ymax,
                const int zoom_level);
    
    int_coordi_t hash(const DataPoint& point);
    
    float recommendedR(const int k);
    
    int cellNum(const int zoom_level);
    
};


class DataPointStorageEntry {
    
public:
    
    DataPoint point;
    int zoom_level;
    
public:
    
    DataPointStorageEntry(DataPoint point, int zoom_level);
};


class DataPointStorage {
    
public:
    
    std::vector<DataPointStorageEntry> dataArray;
    
    std::unordered_map<data_id_t, data_id_t> uid_map;
    
    
public: // update operations
    
    void insertDataPoint(DataPoint point, int zoom_level);
    
    void updateZoomLevel(data_id_t uid, int zoom_level);

    void shuffleStorage();
    
    
public: // read operations

    DataPointStorage();
    
    // read only dataPoint with zoom_level not larger than what's specified.
    virtual DataPoint* readNext(const int zoom_level = 0);
    
    virtual void rewind();
    
private: // read operations
    
    data_id_t _readPointer;
    
};


// Main class for Multi-stage PAS (Perception-Aware Sampling)
// Notes:
//   1. The initial data are provided by DataSource, and this class access them
//      using readNext() and rewind().
//   2. The initial data size could be very large, so this class only stores the
//      data sampled in the first stage in DataPointStorage.
//   3. If a requested zoom-level is 10, there will be 10 sampling processes
//      including the initial stage.
//   4. The number of partitions (or cells) is 4^l at zoom level l.
class MultiStageSampler {
    
private:
    
    const int zoom_levels;    // total zoom-levels
    const int sweep_num;      // sweep number over data source
    const float sample_prob;  // mixed with random sampling
    
    
public:
    
    DataPointStorage storage;
    
    
public:
    
    // 'zoom_level' indicates a total zoom-levels that the final results should
    // retain. The zoom-levels are stored as an entry in DataPointStorage
    // together with corresponding DataPoints.
    //
    // 'sweep_num' says how many time to read the data source for a single
    // sampling process. the larger the better, but it will take more time.
    MultiStageSampler(const int zoom_levels, const int sweep_num, const float sample_prob);
    
    // 'k' is the sample size for each level. There are fixed.
    void sample(DataSource& initialSource, const int k, const float alpha, const float beta);
    
    DataPointStorage& getStorage();
    
};


void MultiStageSampleWrapper(const std::string inputFileName,
                             const std::string outputFileName,
                             const float alpha, const float beta, const int k, const std::vector<float>& range,
                             const int zoom_levels, const int sweep_num, const float sample_prob);


#endif /* defined(__pas__MultiStage__) */
