//
//  SingleStage.cpp
//  pas
//
//  Created by Yongjoo Park on 11/29/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include "DataSource.h"
#include "RandomSampling.h"
#include "SingleStage.h"
#include <cfloat>
#include <cstdlib>
#include <ctime>
#include <ctgmath>
#include <spatialindex/SpatialIndex.h>
#include <spatialindex/Point.h>
#include <spatialindex/capi/IdVisitor.h>
#include <sstream>
#include <vector>


using namespace std;
using namespace SpatialIndex;


float recommendedR(const float xmin, const float xmax, const float ymin, const float ymax, const int k)
{
    cout << "WARN: We need a better mechanism to determine the radius." << endl;
    float xr = (xmax - xmin) / sqrt(k) / 2.0;
    float yr = (ymax - ymin) / sqrt(k) / 2.0;

    return max(xr, yr) / 100;
}


void writeSampledTo(const std::string outputFileName, const std::vector<DataPoint>& sampled) {
    ofstream ofs;
    ofs.open(outputFileName);

    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << ",0" << endl;
    }

    ofs.close();
}


// Output files whenever a single sweep ends
void GradualSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup) {

    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    float r = recommendedR(xmin, xmax, ymin, ymax, k);;

    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "alpha: " << alpha << ", beta: " << beta << ", sample size: " << k << endl;
    cout << "sweep num: " << sweep_num << ", sample prob: " << sample_prob << ", r: " << r << endl;
    cout << "range: " << xmin << " " << xmax << " " << ymin << " " << ymax << endl;

    // Sampling
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    PASSampler sampler(r, alpha, beta, k);

    if (warmup) {
        cout << "warm sampler" << endl;
        sampler.warm(fileSource);
    }
    
    cout << "actual routine starts" << endl;
    for (int i = 0; i < sweep_num; i++) {
        clock_t begin = clock();

        DataPoint* point;

        // single sweep
        long counter = 0;

        while ((point = fileSource.readNext())) {
            float rn = ((float) rand()) / RAND_MAX;
            if (rn <= sample_prob) {
                sampler.check(*point);
            }
            delete point;

            if (++counter % 1000 == 0) {
                cout << "\r  counter: " << counter << flush;
            }
        }

        fileSource.rewind();

        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

        // output this run's status
        vector<DataPoint> sampled = sampler.getSampled();
        std::ostringstream oss;
        oss << outputFileName << i;
        string thisrunOutputFileName = oss.str();
        writeSampledTo(thisrunOutputFileName, sampled);

        cout << "; Sweep " << i << " Done; " << elapsed_secs << " secs taken." << endl;
    }

    cout << "Done." << endl << endl;
}


void SingleStageSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup) {

    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    float r = recommendedR(xmin, xmax, ymin, ymax, k);;

    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "alpha: " << alpha << ", beta: " << beta << ", sample size: " << k << endl;
    cout << "sweep num: " << sweep_num << ", sample prob: " << sample_prob << ", r: " << r << endl;
    cout << "range: " << xmin << " " << xmax << " " << ymin << " " << ymax << endl;

    // Sampling
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    PASSampler sampler(r, alpha, beta, k);

    if (warmup) {
        cout << "warm sampler" << endl;
        sampler.warm(fileSource);
    }
    
    cout << "actual routine starts" << endl;
    for (int i = 0; i < sweep_num; i++) {
        DataPoint* point;

        // single sweep
        long counter = 0;

        while ((point = fileSource.readNext())) {
            float rn = ((float) rand()) / RAND_MAX;
            if (rn <= sample_prob) {
                sampler.check(*point);
            }
            delete point;

            if (++counter % 1000 == 0) {
                cout << "\r  counter: " << counter << flush;
            }
        }

        fileSource.rewind();

        cout << "; Sweep " << i << " Done." << endl;
    }
    
    
    // Output
    ofstream ofs;
    ofs.open(outputFileName);
    
    vector<DataPoint> sampled = sampler.getSampled();
    
    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << ",0" << endl;
    }
    
    ofs.close();

    cout << "Done." << endl << endl;
}



#pragma mark RespPoint

RespPoint::RespPoint() {}


RespPoint::RespPoint(DataPoint point, float resp)
: point(point), resp(resp), qid(0)
{
    
}



#pragma mark CustomQueue

int CustomQueue::getLeftChildIndex(const int dataIndex) const {
    int one_start_record_index = dataIndex + 1;
    int one_start_left_child_index = one_start_record_index * 2;
    return one_start_left_child_index - 1;
}


int CustomQueue::getRightChildIndex(const int dataIndex) const {
    int one_start_record_index = dataIndex + 1;
    int one_start_right_child_index = one_start_record_index * 2 + 1;
    return one_start_right_child_index - 1;
}


int CustomQueue::getParentIndex(const int dataIndex) const {
    int one_start_record_index = dataIndex + 1;
    int one_start_parent_index = one_start_record_index / 2;
    return one_start_parent_index - 1;
}


void CustomQueue::updateQid(const int dataIndex) {
    RespPoint& rp = _dataArray[dataIndex];
    rp.qid = dataIndex;
    uid_map[rp.point.uid] = dataIndex;
}


void CustomQueue::insert(RespPoint& rp) {
    _dataArray.push_back(rp);
    updateQid(_dataArray.size()-1);
    
    raiseIfNeeded(_dataArray.size()-1);
}


RespPoint CustomQueue::pop() {
    RespPoint rp = _dataArray[0];
    
    int lastPointIndex = (int) _dataArray.size() - 1;
    _dataArray[0] = _dataArray[lastPointIndex];
    updateQid(0);
    _dataArray.erase(_dataArray.begin() + lastPointIndex);
    
    lowerIfNeeded(0);

    uid_map.erase(rp.point.uid);
    
    return rp;
}


void CustomQueue::raiseIfNeeded(const int dataIndex) {
    // top is the item with the smallest resp
    
    // stop cases
    if (dataIndex == 0)
        return;
    
    const int parentIndex = getParentIndex(dataIndex);
    
    if (getResp(parentIndex) <= getResp(dataIndex))
        return;
    
    // otherwise, we should perform swapping
    RespPoint parent_rp = _dataArray[parentIndex];
    RespPoint child_rp = _dataArray[dataIndex];
    
    _dataArray[dataIndex] = parent_rp;  updateQid(dataIndex);
    _dataArray[parentIndex] = child_rp; updateQid(parentIndex);
    
    raiseIfNeeded(parentIndex);
}


void CustomQueue::lowerIfNeeded(const int dataIndex) {
    // top is the item with the smallest resp
    
    // stop cases
    const int leftChildIndex = getLeftChildIndex(dataIndex);
    const int rightChildIndex = getRightChildIndex(dataIndex);
    const int arrayLastIndex = (int) _dataArray.size() - 1;
    
    if (leftChildIndex > arrayLastIndex)
        return;
    
    if (rightChildIndex <= arrayLastIndex) {
        // double childs case

        if (getResp(leftChildIndex) < getResp(rightChildIndex)) {
            if (getResp(dataIndex) <= getResp(leftChildIndex)) return;

            // perform swap with the left child.
            RespPoint parent_rp = _dataArray[dataIndex];
            RespPoint leftChild_rp = _dataArray[leftChildIndex];
            
            _dataArray[dataIndex] = leftChild_rp;   updateQid(dataIndex);
            _dataArray[leftChildIndex] = parent_rp; updateQid(leftChildIndex);
            
            lowerIfNeeded(leftChildIndex);
        }
        else {
            if (getResp(dataIndex) <= getResp(rightChildIndex)) return;

            // perform swap with the right child
            RespPoint parent_rp = _dataArray[dataIndex];
            RespPoint rightChild_rp = _dataArray[rightChildIndex];
            
            _dataArray[dataIndex] = rightChild_rp;   updateQid(dataIndex);
            _dataArray[rightChildIndex] = parent_rp; updateQid(rightChildIndex);
            
            lowerIfNeeded(rightChildIndex);
        }
    }
    else if (leftChildIndex == arrayLastIndex) {
        // single child case

        if (getResp(dataIndex) <= getResp(leftChildIndex)) return;

        RespPoint parent_rp = _dataArray[dataIndex];
        RespPoint leftChild_rp = _dataArray[leftChildIndex];
        
        _dataArray[dataIndex] = leftChild_rp;   updateQid(dataIndex);
        _dataArray[leftChildIndex] = parent_rp; updateQid(leftChildIndex);
        
        lowerIfNeeded(leftChildIndex);
    }
}


RespPoint& CustomQueue::get(data_id_t uid)
{
    return _dataArray[uid_map[uid]];
}


float CustomQueue::getResp(const int dataIndex) const
{
    return _dataArray[dataIndex].resp;
}



#pragma mark Others

float circleIntersect(const float r, const float d) {
    //if (d >= 2*r) {
    //    return 0;
    //}
    //else {
    //    return 1 - d / 2*r;
    //}

    return exp(-pow(d/r,2));
}


float circleIntersect(const DataPoint& p1, const DataPoint& p2, const float r) {
    float d = sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
    return circleIntersect(r, d);
}



#pragma mark PASSampler

PASSampler::PASSampler(float r, float alpha, float beta, int k)
: r(r), alpha(alpha), beta(beta), k(k)
{
    memfile = StorageManager::createNewMemoryStorageManager();
    
    id_type indexIdentifier;
    int indexCap = 10;
    int leafCap = 100;
    int dim = 2;
    tree = RTree::createNewRTree(*memfile, 0.7,
                                 indexCap, leafCap, dim,
                                 RTree::RV_RSTAR, indexIdentifier);
}


void PASSampler::warm(DataSource& source) {

    int strata_num = k;
    StratifiedSampler sampler(strata_num, k);

    sampler.sample(source);

    vector<DataPoint> sampled = sampler.getSampled();

    for (vector<DataPoint>::const_iterator it = sampled.begin();
            it != sampled.end(); it++) {
        check(*it);
    }

    source.rewind();
}


void PASSampler::check(const DataPoint& point)
{
    if (queue.uid_map.count(point.uid) > 0)
        return;
    
    
    if (queue._dataArray.size() < k) {
        Expand(point);
    }
    else {
        Expand(point);
        Shrink(point);
    }
}


void PASSampler::pseudoCheck(const DataPoint& point)
{
    // I do not recall why I introduced this function. I block this function to
    // examine this further later when needed.
    assert(false);

#if 0
    if (queue.uid_map.count(point.uid) > 0)
        return;
    
    
    if (queue._dataArray.size() < k) {
        Expand(point);
    }
    else {
        // Pseudo-Operation
        double pLow[] = {point.x - 2*r, point.y - 2*r};
        double pHigh[] = {point.x + 2*r, point.y + 2*r};
        Region region(pLow, pHigh, 2);
        
        IdVisitor vis;
        tree->intersectsWithQuery(region, vis);
        
        std::vector<uint64_t> results = vis.GetResults();

        if (results.size() > 0) return;

        // Real
        Expand(point);
        Shrink();
    }
#endif

}


#if 0
void PASSampler::Expand(const DataPoint& point) {

    RespPoint rp(point, alpha * point.w);

    for (vector<RespPoint>::iterator it = queue._dataArray.begin();
            it != queue._dataArray.end(); it++) {
        RespPoint& s = *it;

        float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
        rp.resp -= l;
        s.resp -= l;
    }

    queue._dataArray.push_back(rp);
}


void PASSampler::Shrink() {
    float min_resp = FLT_MAX;
    int min_index = 0;

    for (int i = 0; i < queue._dataArray.size(); i++) {
        RespPoint& s = queue._dataArray[i];

        if (s.resp < min_resp) {
            min_resp = s.resp;
            min_index = i;
        }
    }

    RespPoint& rp = queue._dataArray[min_index];

    for (vector<RespPoint>::iterator it = queue._dataArray.begin();
            it != queue._dataArray.end(); it++) {
        RespPoint& s = *it;

        float l = 0.5 * beta * rp.point.w * s.point.w * circleIntersect(s.point, rp.point, r);
        s.resp += l;
    }

    queue._dataArray[min_index] = queue._dataArray[queue._dataArray.size() - 1];
    queue._dataArray.erase(queue._dataArray.begin() + (queue._dataArray.size() - 1));
}
#endif



void PASSampler::Expand(const DataPoint& point) {
    // compute responsibility of 'point' in an expanded set.
    // also update the responsibilities of the items already in the set.

    // get all the points within the distance '2*r' from point.
    double pLow[] = {point.x - 2*r, point.y - 2*r};
    double pHigh[] = {point.x + 2*r, point.y + 2*r};
    Region region(pLow, pHigh, 2);
    
    IdVisitor vis;
    tree->intersectsWithQuery(region, vis);
    
    std::vector<uint64_t> results = vis.GetResults();

    RespPoint rp(point, alpha * point.w);
    
    for (std::vector<uint64_t>::const_iterator it = results.begin();
         it != results.end(); it++) {
        data_id_t uid = *it;
        
        RespPoint& s = queue.get(uid);
        
        float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
        rp.resp -= l;
        s.resp -= l;

        // due to the change of s.resp, we should adjust the queue
        queue.raiseIfNeeded(s.qid);
    }

    //int qid_expect = 0;
    //for (vector<RespPoint>::iterator it = queue._dataArray.begin();
    //        it != queue._dataArray.end(); it++) {
    //    RespPoint& s = *it;

    //    float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
    //    rp.resp -= l;
    //    s.resp -= l;

    //    assert(qid_expect == s.qid);
    //    queue.raiseIfNeeded(s.qid);

    //    qid_expect++;
    //}

    //    put into queue for ordering
    //    put into database to reference data information with uid; not anymore
    //    put into R-tree for range search
    queue.insert(rp);

    //sampleBase.insert(make_pair(rp.point.uid, rp));
    
    double coordi[] = {rp.point.x, rp.point.y};
    Point p(coordi, 2);
    tree->insertData(0, NULL, p, rp.point.uid);
}


// param not used
void PASSampler::Shrink(const DataPoint& added_point) {

    RespPoint popped = queue.pop();
    DataPoint& point = popped.point;

    // remove the popped point from rtree
    double coordi[] = {point.x, point.y};
    Point p(coordi, 2);
    tree->deleteData(p, point.uid);

    // We are going to restore the responsibilities
    // get all the points within the distance '2*r' from point.
    double pLow[] = {point.x - 2*r, point.y - 2*r};
    double pHigh[] = {point.x + 2*r, point.y + 2*r};
    Region region(pLow, pHigh, 2);
    
    IdVisitor vis;
    tree->intersectsWithQuery(region, vis);
    
    std::vector<uint64_t> results = vis.GetResults();
    
    for (std::vector<uint64_t>::const_iterator it = results.begin();
         it != results.end(); it++) {
        data_id_t uid = *it;
        RespPoint& s = queue.get(uid);

        float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
        s.resp += l;

        // due to the change of s.resp, we should adjust the queue
        queue.lowerIfNeeded(s.qid);
    }

    //assert(queue.uid_map.size() == k + 1);

    //for (auto it = queue.uid_map.begin(); it != queue.uid_map.end(); it++) {
    //    int dataIndex = it->second;
    //    RespPoint& s = queue._dataArray[dataIndex];

    //    float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
    //    s.resp += l;
    //    queue.lowerIfNeeded(s.qid);
    //}

    //int qid_expect = 0;
    //for (vector<RespPoint>::iterator it = queue._dataArray.begin();
    //        it != queue._dataArray.end(); it++) {

    //    RespPoint& s = *it;

    //    float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
    //    s.resp += l;

    //    assert(qid_expect == s.qid);
    //    //queue.lowerIfNeeded(s.qid);

    //    qid_expect++;
    //}
    
}


vector<DataPoint> PASSampler::getSampled()
{
    vector<DataPoint> sampled;

    for (vector<RespPoint>::const_iterator it = queue._dataArray.begin();
            it != queue._dataArray.end(); it++) {
        sampled.push_back(it->point);
    }
    
    //for (auto it = sampleBase.begin(); it != sampleBase.end(); it++) {
    //    sampled.push_back(it->second.point);
    //}
    
    return sampled;
}


PASSampler::~PASSampler()
{
    delete tree;
    delete memfile;
}





