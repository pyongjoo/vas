#include "DataSource.h"
#include "RandomSampling.h"
#include "SingleStage.h"
#include "ApproxSingleStage.h"
#include <algorithm>
#include <cfloat>
#include <cstdlib>
#include <ctime>
#include <ctgmath>
#include <spatialindex/SpatialIndex.h>
#include <spatialindex/Point.h>
#include <spatialindex/capi/IdVisitor.h>
#include <sstream>
#include <vector>
#include <utility>


using namespace std;
using namespace SpatialIndex;


DataPointWithNeighbor::DataPointWithNeighbor() {
}

DataPointWithNeighbor::DataPointWithNeighbor(DataPoint point, vector<data_id_t> neighbors)
: point(point), neighbors(unordered_map<data_id_t, int>(100))
{
    for (vector<data_id_t>::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
        data_id_t uid = *it;
        this->neighbors.insert(make_pair(uid, 0));
    }
}


void ApproxSingleStageSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup) {

    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    float r = recommendedR(xmin, xmax, ymin, ymax, k);;

    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "alpha: " << alpha << ", beta: " << beta << ", sample size: " << k << endl;
    cout << "sweep num: " << sweep_num << ", sample prob: " << sample_prob << ", r: " << r << endl;
    cout << "range: " << xmin << " " << xmax << " " << ymin << " " << ymax << endl;

    // Sampling
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    ApproxPASSampler sampler(r, alpha, beta, k);

    if (warmup) {
        cout << "warm sampler" << endl;
        sampler.warm(fileSource);
    }
    
    cout << "actual routine starts" << endl;
    for (int i = 0; i < sweep_num; i++) {
        DataPoint* point;

        // single sweep
        long counter = 0;

        while ((point = fileSource.readNext())) {
            float rn = ((float) rand()) / RAND_MAX;
            if (rn <= sample_prob) {
                sampler.check(*point);
            }
            delete point;

            if (++counter % 1000 == 0) {
                cout << "\r  counter: " << counter << flush;
            }
        }

        fileSource.rewind();

        cout << "; Sweep " << i << " Done." << endl;
    }
    
    
    // Output
    ofstream ofs;
    ofs.open(outputFileName);
    
    vector<DataPoint> sampled = sampler.getSampled();
    
    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << ",0" << endl;
    }
    
    ofs.close();

    cout << "Done." << endl << endl;
}


ApproxPASSampler::ApproxPASSampler(float r, float alpha, float beta, int k) : PASSampler(r, alpha, beta, k) { 
    points_map = new unordered_map<data_id_t, DataPointWithNeighbor>(15000000);
}

ApproxPASSampler::~ApproxPASSampler() {
    delete points_map;
}


void ApproxPASSampler::warm(DataSource& source) {

    int strata_num = k;
    StratifiedSampler sampler(strata_num, k);

    sampler.sample(source);

    vector<DataPoint> sampled = sampler.getSampled();

    for (vector<DataPoint>::const_iterator it = sampled.begin();
            it != sampled.end(); it++) {
        check(*it);
    }

    source.rewind();
}


void ApproxPASSampler::check(const DataPoint& point)
{
    if (points_map->count(point.uid) > 0)
        return;
    
    
    if (points_map->size() < k) {
        Expand(point);
    }
    else {
        ExpandAndShrink(point);
        //Shrink(point);
    }
}


void ApproxPASSampler::Expand(const DataPoint& point) {
    // compute responsibility of 'point' in an expanded set.
    // also update the responsibilities of the items already in the set.

    // retrieve the neighbors to update their responsibility (which is count)
    double pLow[] = {point.x - 2*r, point.y - 2*r};
    double pHigh[] = {point.x + 2*r, point.y + 2*r};
    Region region(pLow, pHigh, 2);

    IdVisitor vis;
    tree->intersectsWithQuery(region, vis);
    std::vector<uint64_t> results = vis.GetResults();

    // update the responsibilities of neighbors.
    for (vector<uint64_t>::const_iterator it = results.begin(); it != results.end(); it++) {
        data_id_t uid = *it;
        points_map->at(uid).neighbors.insert(make_pair(point.uid, 0));
    }

    // insert data into R-tree and hash table.
    double coordi[] = {point.x, point.y};
    Point p(coordi, 2);
    tree->insertData(0, NULL, p, point.uid);

    DataPointWithNeighbor pn(point, results);
    points_map->insert(make_pair(point.uid, pn));
}


void ApproxPASSampler::ExpandAndShrink(const DataPoint& point) {
    // compute responsibility of 'point' in an expanded set.
    // also update the responsibilities of the items already in the set.


    // retrieve the neighbors to update their responsibility (which is count)
    double pLow[] = {point.x - 2*r, point.y - 2*r};
    double pHigh[] = {point.x + 2*r, point.y + 2*r};
    Region region(pLow, pHigh, 2);

    IdVisitor vis;
    tree->intersectsWithQuery(region, vis);
    std::vector<uint64_t> results = vis.GetResults();

    // record the one with the maximum neighbor size.
    bool updated = false;
    int max_results_count = results.size();
    data_id_t uid_to_remove = point.uid;

    // update the responsibilities of neighbors.
    for (vector<uint64_t>::const_iterator it = results.begin(); it != results.end(); it++) {
        data_id_t uid = *it;
        points_map->at(uid).neighbors.insert(make_pair(point.uid, 0));

        if (points_map->at(uid).neighbors.size() > max_results_count) {
            max_results_count = points_map->at(uid).neighbors.size();
            uid_to_remove = uid;
            updated = true;
        }
    }

    // insert the new point into R-tree and hash table.
    {
        double coordi[] = {point.x, point.y};
        Point p(coordi, 2);
        tree->insertData(0, NULL, p, point.uid);

        DataPointWithNeighbor pn(point, results);
        points_map->insert(make_pair(point.uid, pn));
    }

    // update the responsibilities of the neighbors of the to-be-removed
    DataPointWithNeighbor pn = points_map->at(uid_to_remove);       // point to remove
    for (unordered_map<data_id_t,int>::const_iterator it = pn.neighbors.begin(); it != pn.neighbors.end(); it++) {
        data_id_t nid = it->first;          // neighbor's uid
        points_map->at(nid).neighbors.erase(uid_to_remove);
    }

    // remove the point from rtree
    double coordi[] = {pn.point.x, pn.point.y};
    Point p(coordi, 2);
    tree->deleteData(p, uid_to_remove);

    points_map->erase(uid_to_remove);
}


#if 0
void ApproxPASSampler::Shrink(const DataPoint& added_point) {

    // get all data within distance 2*r
    double pLow[] = {added_point.x - 2*r, added_point.y - 2*r};
    double pHigh[] = {added_point.x + 2*r, added_point.y + 2*r};
    Region region(pLow, pHigh, 2);
    
    IdVisitor vis;
    tree->intersectsWithQuery(region, vis);
    std::vector<uint64_t> results = vis.GetResults();
    std::vector<uint64_t> sampled_results;

    int scount = 10;
    if (results.size() <= scount)
        sampled_results = results;
    else {
        random_shuffle(results.begin(), results.end());
        for (int i = 0; i < scount; i++) {
            sampled_results.push_back(results[i]);
        }
    }

    int max_results_count = results.size();
    const DataPoint* point_to_remove = &added_point;      // this never holds new()-ed object.

    for (std::vector<uint64_t>::const_iterator it = sampled_results.begin(); it != sampled_results.end(); it++) {
        data_id_t uid = *it;
        DataPoint& candidate_point = points_map[uid];

        // check the neighbor count
        double pLow[] = {candidate_point.x - 2*r, candidate_point.y - 2*r};
        double pHigh[] = {candidate_point.x + 2*r, candidate_point.y + 2*r};
        Region cand_region(pLow, pHigh, 2);

        IdVisitor cand_vis;
        tree->intersectsWithQuery(cand_region, cand_vis);
        std::vector<uint64_t> cand_results = vis.GetResults();

        if (cand_results.size() > max_results_count) {
            max_results_count = cand_results.size();
            point_to_remove = &candidate_point;
        }
    }


    // remove the popped point from rtree
    double coordi[] = {point_to_remove->x, point_to_remove->y};
    Point p(coordi, 2);
    tree->deleteData(p, point_to_remove->uid);
    points_map.erase(point_to_remove->uid);
}
#endif


vector<DataPoint> ApproxPASSampler::getSampled() {

    vector<DataPoint> sampled;

    for (unordered_map<data_id_t, DataPointWithNeighbor>::const_iterator it = points_map->begin(); it != points_map->end(); it++) {
        DataPointWithNeighbor pn = it->second;
        sampled.push_back(pn.point);
    }

    return sampled;
}


