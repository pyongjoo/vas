//
//  MultiStage.cpp
//  pas
//
//  Created by Yongjoo Park on 11/29/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <ctgmath>
#include <vector>
#include "DataSource.h"
#include "MultiStage.h"
#include "SingleStage.h"


using namespace std;


void MultiStageSampleWrapper(const std::string inputFileName,
                             const std::string outputFileName,
                             const float alpha, const float beta, const int k, const vector<float>& range,
                             const int zoom_levels, const int sweep_num, const float sample_prob)
{
    assert(range.size() == 4);
    
    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    assert(xmin < xmax);
    assert(ymin < ymax);

    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "zoom levels: " << zoom_levels << endl;
    cout << "alpha: " << alpha << ", beta: " << beta << ", sample size: " << k << endl;
    cout << "sweep num: " << sweep_num << ", sample prob: " << sample_prob << endl;
    

    // sample multiple levels.
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    
    MultiStageSampler sampler(zoom_levels, sweep_num, sample_prob);
    sampler.sample(fileSource, k, alpha, beta);

    DataPointStorage& storage = sampler.getStorage();
    
    // output to file
    ofstream ofs;
    ofs.open(outputFileName);
    
    vector<DataPointStorageEntry>& dataArray = storage.dataArray;
    
    for (vector<DataPointStorageEntry>::const_iterator it = dataArray.begin();
         it != dataArray.end(); it++) {
        // output a single line
        const DataPoint& point = it->point;
        const int zoom_level = it->zoom_level;
        
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << "," << zoom_level << endl;
    }
    
    ofs.close();

    cout << "Done." << endl << endl;
}




#pragma mark Partitioner


Partitioner::Partitioner(const float xmin, const float xmax, const float ymin, const float ymax, const int zoom_level)
: xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax), zoom_level(zoom_level)
{
    
}


int_coordi_t Partitioner::hash(const DataPoint& point)
{
    int cell_num = cellNum(zoom_level);
    float width = (xmax - xmin) / cell_num;
    float height = (ymax - ymin) / cell_num;
    
    int x_coordi = floor((point.x - xmin) / width);
    x_coordi = min(x_coordi, cell_num - 1);
    
    int y_coordi = floor((point.y - ymin) / height);
    y_coordi = min(y_coordi, cell_num - 1);
    
    return make_pair(x_coordi, y_coordi);
}


int Partitioner::cellNum(const int zoom_level) {
    return pow(2, zoom_level);
}


float Partitioner::recommendedR(const int k)
{
    int cell_num = cellNum(zoom_level);
    float xr = (xmax - xmin) / cell_num / sqrt(k) / 2.0;
    float yr = (ymax - ymin) / cell_num / sqrt(k) / 2.0;
    
    return min(xr, yr);
}


#pragma mark DataPointStorageEntry and DataPointStorage


DataPointStorageEntry::DataPointStorageEntry(DataPoint point, int zoom_level)
: point(point), zoom_level(zoom_level)
{
    
}


DataPointStorage::DataPointStorage() : _readPointer(0) {}


void DataPointStorage::insertDataPoint(DataPoint point, int zoom_level)
{
    dataArray.push_back(DataPointStorageEntry(point, zoom_level));
    
    uid_map.insert(make_pair(point.uid, dataArray.size() - 1));
}


void DataPointStorage::updateZoomLevel(data_id_t uid, int zoom_level)
{
    data_id_t local_id = uid_map[uid];
    
    dataArray[local_id].zoom_level = zoom_level;
}


void DataPointStorage::shuffleStorage()
{
    random_shuffle(dataArray.begin(), dataArray.end());

    uid_map.clear();

    for (int i = 0; i < dataArray.size(); i++) {
        DataPointStorageEntry& entry = dataArray[i];
        uid_map.insert(make_pair(entry.point.uid, i));
    }
}


DataPoint* DataPointStorage::readNext(const int zoom_level)
{
    DataPoint* point = new DataPoint();
    
    while (true) {
        if (_readPointer >= dataArray.size())
            return NULL;
        
        DataPointStorageEntry d = dataArray[_readPointer++];
        
        if (d.zoom_level != zoom_level)
            continue;
        
        point->x = d.point.x;
        point->y = d.point.y;
        point->z = d.point.z;
        point->w = d.point.w;
        point->uid = d.point.uid;
        
        return point;
    }
}


void DataPointStorage::rewind() {
    _readPointer = 0;
}


#pragma mark MultiStageSampler

MultiStageSampler::MultiStageSampler(const int zoom_levels, const int sweep_num, const float sample_prob)
: zoom_levels(zoom_levels), sweep_num(sweep_num), sample_prob(sample_prob)
{
    assert(zoom_levels >= 0);
}


void MultiStageSampler::sample(DataSource& initialSource, const int k, const float alpha, const float beta)
{
    const float xmin = initialSource.xmin;
    const float xmax = initialSource.xmax;
    const float ymin = initialSource.ymin;
    const float ymax = initialSource.ymax;
    
    // zoom level start from zoom_levels-1
    // because whlie loop includes '--' operator, we set this to zoom_levels.
    int zoom_level = zoom_levels;
    
    
    // Sampling for different zoom levels.
    while (--zoom_level >= 0) {
        Partitioner partitioner(xmin, xmax, ymin, ymax, zoom_level);
        
        float r = partitioner.recommendedR(k);
        
        // create a matrix of samplers
        int cell_num = partitioner.cellNum(zoom_level);
        vector<vector<PASSampler*> > samplerMatrix;
        
        for (int i = 0; i < cell_num; i++) {
            vector<PASSampler*> samplerArray;
            
            for (int j = 0; j < cell_num; j++) {
                PASSampler* a_sampler = new PASSampler(r, alpha, beta, k);
                samplerArray.push_back(a_sampler);
            }
            
            samplerMatrix.push_back(samplerArray);
        }
        
        
        // sampling process
        if (zoom_level == zoom_levels - 1) {
            // initial sampling

            for (int i = 0; i < sweep_num + 1; i++) {
                
                DataPoint* point;

                long counter = 0;
                
                while ((point = initialSource.readNext())) {
                    float rn = ((float) rand()) / RAND_MAX;
                    //if (rn <= sample_prob) {
                    if (i == 0 && rn <= 0.01) {
                        int_coordi_t sampler_coordi = partitioner.hash(*point);
                        samplerMatrix[sampler_coordi.first][sampler_coordi.second]->pseudoCheck(*point);
                    }
                    else if (i > 0 && rn <= sample_prob) {
                        int_coordi_t sampler_coordi = partitioner.hash(*point);
                        samplerMatrix[sampler_coordi.first][sampler_coordi.second]->check(*point);
                    }
                    delete point;

                    if (++counter % 1000 == 0) {
                        cout << "\r    counter: " << counter << flush;
                    }
                }
                
                initialSource.rewind();

                cout << "    Sweep " << i << " Done." << endl;
            }
            
            // Put sampled data points into the local storage
            for (int i = 0; i < cell_num; i++) {
                for (int j = 0; j < cell_num; j++) {
                    PASSampler* cell_sampler = samplerMatrix[i][j];
                    
                    vector<DataPoint> sampled = cell_sampler->getSampled();
                    
                    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
                        DataPoint point = *it;
                        
                        storage.insertDataPoint(point, zoom_level);
                    }
                }
            }

            storage.shuffleStorage();

            cout << "  Initial zoom-level done;  " << storage.dataArray.size() << " number of data points." << endl;
        }
        else {
            // later stages

            for (int i = 0; i < sweep_num; i++) {
                
                DataPoint* point;

                long counter = 0;
                
                while ((point = storage.readNext(zoom_level+1))) {
                    float rn = ((float) rand()) / RAND_MAX;
                    if (rn <= sample_prob) {
                        int_coordi_t sampler_coordi = partitioner.hash(*point);
                        samplerMatrix[sampler_coordi.first][sampler_coordi.second]->check(*point);
                    }
                    delete point;

                    if (++counter % 1000 == 0) {
                        cout << "\r    counter: " << counter << flush;
                    }
                }
                
                storage.rewind();

                cout << "    Sweep " << i << " Done." << endl;
            }

            // update zoom-levels of the sampled data points.
            for (int i = 0; i < cell_num; i++) {
                for (int j = 0; j < cell_num; j++) {
                    PASSampler* cell_sampler = samplerMatrix[i][j];
                    
                    vector<DataPoint> sampled = cell_sampler->getSampled();
                    
                    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
                        DataPoint point = *it;
                        
                        storage.updateZoomLevel(point.uid, zoom_level);
                    }
                }
            }

            cout << "  " << zoom_level << " zoom-level done." << endl;

        }

        // clear samplerMatrix
        for (int i = 0; i < cell_num; i++) {
            for (int j = 0; j < cell_num; j++) {
                delete samplerMatrix[i][j];
            }
        }

    }   // a single stage done (big while loop)
}


DataPointStorage& MultiStageSampler::getStorage() {
    return storage;
}


