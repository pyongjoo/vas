//
//  RandomSampling.h
//  pas
//
//  Created by Yongjoo Park on 12/3/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#ifndef __pas__RandomSampling__
#define __pas__RandomSampling__

#include <iostream>
#include <string>
#include <vector>
#include "DataSource.h"
#include "MultiStage.h"


class RandomSampler {
    
public:
    
    std::vector<DataPoint> dataArray;
    
    const int k;        // sample size
    
    int checked_num;    // how many data points have passed
    
    
public:
    
    RandomSampler(const int k);
    
    void check(DataPoint& point);
    
    std::vector<DataPoint> getSampled();
};


void RandomSamplerWrapper(const std::string inputFileName,
                          const std::string outputFileName,
                          const int k);



class StratifiedSampler {
    
public:
    
    const int k;        // sample size
    
    const int strata_num;    // vertical * horizontal
    
    std::vector<DataPoint> dataArray;
    
//    std::vector<float> range;
    
    int_coordi_t hash(const DataPoint& point, const std::vector<float>& range);
    
    std::vector<std::vector<int> > plan(DataSource& source, std::vector<float>& range);  // determine number of data points for each cell.
    

public:
    
    StratifiedSampler(const int strata_num, const int k);
    
    void sample(DataSource& source);
    
    std::vector<DataPoint> getSampled();
};


void StratifiedSamplerWrapper(const std::string inputFileName,
                          const std::string outputFileName,
                          const int k, const int strata_num, const std::vector<float>& range);


#endif /* defined(__pas__RandomSampling__) */
