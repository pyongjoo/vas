//
//  DataSource.cpp
//  pas
//
//  Created by Yongjoo Park on 11/27/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include "DataSource.h"
#include <cassert>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>


using namespace std;



# pragma mark DataSource

DataSource::DataSource(const float xmin, const float xmax, const float ymin, const float ymax)
: xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax)
{
    
}


#pragma mark FileDataSource

FileDataSource::FileDataSource(const string filename,
                               const float xmin, const float xmax, const float ymin, const float ymax)
: DataSource(xmin, xmax, ymin, ymax), _lineNumber(1)
{
    ifs = new ifstream(filename.c_str(), ifstream::in);
    if (!(ifs->is_open())) {
        cout << "This file should exist: " << filename << endl;
    }
    assert(ifs->is_open());
}


FileDataSource::~FileDataSource()
{
    delete ifs;
}


DataPoint* FileDataSource::parseLine(string& line) {
    
    // parse a line into strings.
    vector<string> tokens;
    
    stringstream linestream(line);
    string token;
    
    while(getline(linestream, token, ','))
    {
        tokens.push_back(token);
    }
    
    DataPoint* point = new DataPoint();
    point->x = atof(tokens[0].c_str());
    point->y = atof(tokens[1].c_str());
    point->z = atof(tokens[2].c_str());
    
    if (tokens.size() >= 4)
        point->w = atof(tokens[3].c_str());
    else
        point->w = 1.0;
    
    return point;
}


DataPoint* FileDataSource::readNext()
{
    string line;
    
    while (true) {
        getline(*ifs, line);
        if (ifs->eof()) return NULL;
        
        DataPoint* point = parseLine(line);
        point->uid = _lineNumber++;
        
        if (point->x < xmin || point->x > xmax || point->y < ymin || point->y > ymax)
            continue;
        else
            return point;
    }
}


void FileDataSource::rewind() {
    ifs->clear();
    ifs->seekg(0, ios::beg);
    _lineNumber = 1;
}



# pragma mark MemoryDataSource

MemoryDataSource::MemoryDataSource(float xmin, float xmax, float ymin, float ymax)
: DataSource(xmin, xmax, ymin, ymax), _lineNumber(0)
{
    
}


void MemoryDataSource::insertData(DataPoint* point) {
    _dataArray.push_back(*point);
}


DataPoint* MemoryDataSource::readNext() {
    DataPoint* point = new DataPoint();
    DataPoint d = _dataArray[_lineNumber++];
    
    point->x = d.x;
    point->y = d.y;
    point->z = d.z;
    point->w = d.w;
    point->uid = d.uid;
    
    return point;
}


void MemoryDataSource::rewind() {
    _lineNumber = 0;
}

