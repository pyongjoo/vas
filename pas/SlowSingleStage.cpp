//
//  SingleStage.cpp
//  pas
//
//  Created by Yongjoo Park on 11/29/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include "DataSource.h"
#include "RandomSampling.h"
#include "SingleStage.h"
#include "SlowSingleStage.h"
#include <cfloat>
#include <cstdlib>
#include <ctime>
#include <ctgmath>
#include <sstream>
#include <vector>


using namespace std;



void SlowSingleStageSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup) {

    const float xmin = range[0];
    const float xmax = range[1];
    const float ymin = range[2];
    const float ymax = range[3];
    
    float r = recommendedR(xmin, xmax, ymin, ymax, k);;

    cout << "Input file: " << inputFileName << endl;
    cout << "Output file: " << outputFileName << endl;
    cout << "alpha: " << alpha << ", beta: " << beta << ", sample size: " << k << endl;
    cout << "sweep num: " << sweep_num << ", sample prob: " << sample_prob << ", r: " << r << endl;
    cout << "range: " << xmin << " " << xmax << " " << ymin << " " << ymax << endl;

    // Sampling
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    PASSampler sampler(r, alpha, beta, k);

    if (warmup) {
        cout << "warm sampler" << endl;
        sampler.warm(fileSource);
    }
    
    cout << "actual routine starts" << endl;
    for (int i = 0; i < sweep_num; i++) {
        DataPoint* point;

        // single sweep
        long counter = 0;

        while ((point = fileSource.readNext())) {
            float rn = ((float) rand()) / RAND_MAX;
            if (rn <= sample_prob) {
                sampler.check(*point);
            }
            delete point;

            if (++counter % 1000 == 0) {
                cout << "\r  counter: " << counter << flush;
            }
        }

        fileSource.rewind();

        cout << "; Sweep " << i << " Done." << endl;
    }
    
    
    // Output
    ofstream ofs;
    ofs.open(outputFileName);
    
    vector<DataPoint> sampled = sampler.getSampled();
    
    for (vector<DataPoint>::const_iterator it = sampled.begin(); it != sampled.end(); it++) {
        DataPoint point = *it;
        ofs << point.x << "," << point.y << "," << point.z
        << "," << point.w << ",0" << endl;
    }
    
    ofs.close();

    cout << "Done." << endl << endl;
}



#pragma mark PASSampler

SlowPASSampler::SlowPASSampler(float r, float alpha, float beta, int k)
: r(r), alpha(alpha), beta(beta), k(k)
{ }


void SlowPASSampler::warm(DataSource& source) {

    int strata_num = k;
    StratifiedSampler sampler(strata_num, k);

    sampler.sample(source);

    vector<DataPoint> sampled = sampler.getSampled();

    for (vector<DataPoint>::const_iterator it = sampled.begin();
            it != sampled.end(); it++) {
        check(*it);
    }

    source.rewind();
}


void SlowPASSampler::check(const DataPoint& point)
{
    if (queue.uid_map.count(point.uid) > 0)
        return;
    
    
    if (queue._dataArray.size() < k) {
        Expand(point);
    }
    else {
        Expand(point);
        Shrink(point);
    }
}


void SlowPASSampler::Expand(const DataPoint& point) {
    // compute responsibility of 'point' in an expanded set.
    // also update the responsibilities of the items already in the set.

    vector<RespPoint> results = queue._dataArray;

    RespPoint rp(point, alpha * point.w);

    for (vector<RespPoint>::const_iterator it = results.begin(); it != results.end(); it++) {

        const RespPoint& a = *it;
        RespPoint& s = queue.get(a.point.uid);

        float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
        rp.resp -= l;
        s.resp -= l;

        // due to the change of s.resp, we should adjust the queue
        queue.raiseIfNeeded(s.qid);
    }

    //    put into queue for ordering
    queue.insert(rp);
}


// param not used
void SlowPASSampler::Shrink(const DataPoint& added_point) {

    RespPoint popped = queue.pop();
    DataPoint& point = popped.point;

    vector<RespPoint> results = queue._dataArray;
    
    for (vector<RespPoint>::const_iterator it = results.begin();
         it != results.end(); it++) {

        const RespPoint& a = *it;
        RespPoint& s = queue.get(a.point.uid);

        float l = 0.5 * beta * point.w * s.point.w * circleIntersect(s.point, point, r);
        s.resp += l;

        // due to the change of s.resp, we should adjust the queue
        queue.lowerIfNeeded(s.qid);
    }
}


vector<DataPoint> SlowPASSampler::getSampled()
{
    vector<DataPoint> sampled;

    for (vector<RespPoint>::const_iterator it = queue._dataArray.begin();
            it != queue._dataArray.end(); it++) {
        sampled.push_back(it->point);
    }

    return sampled;
}


SlowPASSampler::~SlowPASSampler()
{ }


