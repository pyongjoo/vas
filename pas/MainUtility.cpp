//
//  MainUtility.cpp
//  pas
//
//  Created by Yongjoo Park on 12/3/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctgmath>
#include <spatialindex/SpatialIndex.h>
#include <spatialindex/Point.h>
#include <spatialindex/capi/IdVisitor.h>
#include <unordered_map>
#include "DataSource.h"


using namespace std;
using namespace SpatialIndex;



class mypoint {
public:
    double x;
    double y;
    
public:
    mypoint(double x, double y) : x(x), y(y) {}
};



mypoint randomPoint(const vector<double>& range, const double r) {
    assert(range.size() == 4);
    
    double xmin = range[0] + r;
    double xmax = range[1] - r;
    double ymin = range[2] + r;
    double ymax = range[3] - r;
    
    double xrand = (xmax - xmin) * (double(rand()) / RAND_MAX) + xmin;
    double yrand = (ymax - ymin) * (double(rand()) / RAND_MAX) + ymin;
    
    return mypoint(xrand, yrand);
}



int countWithin(const vector<uint64_t>& results, mypoint rpoint,
                const unordered_map<long, long>& uid_map, const vector<DataPoint>& dataArray, const double r)
{
    int count = 0;
    
    for (vector<uint64_t>::const_iterator it = results.begin();
         it != results.end(); it++) {
        long uid = *it;
        const DataPoint& point = dataArray[uid_map.at(uid)];
        
        double d = sqrt( pow(point.x - rpoint.x, 2) + pow(point.y - rpoint.y, 2));
        
        if (d <= r) count++;
    }
    
    return count;
}


double utility(long sample_num, double param = 0.1) {
    return 1 - exp(-param*sample_num);
}


double compute_average_utility(vector<DataPoint>& dataArray,
        const vector<double>& range, const double r, const double param)
{
    
    // prepare r-tree
    IStorageManager* memfile = StorageManager::createNewMemoryStorageManager();
    
    id_type indexIdentifier;
    int indexCap = 10;
    int leafCap = 100;
    int dim = 2;
    ISpatialIndex* tree = RTree::createNewRTree(*memfile, 0.7,
                                 indexCap, leafCap, dim,
                                 RTree::RV_RSTAR, indexIdentifier);
    
    
    // insert points
    for (vector<DataPoint>::const_iterator it = dataArray.begin();
         it != dataArray.end(); it++) {
        
        const DataPoint& point = *it;
        
        double coordi[] = {point.x, point.y};
        Point p(coordi, 2);
        tree->insertData(0, NULL, p, point.uid);
    }
    
    
    // UID to index map
    unordered_map<long, long> uid_map;
    long index = 0;
    for (vector<DataPoint>::const_iterator it = dataArray.begin();
         it != dataArray.end(); it++) {
        
        const DataPoint& point = *it;
        uid_map.insert(make_pair(point.uid, index));
        index++;
    }
    
    
    // query step
    int query_num = 5000;
    int valid_num = 0;
    double total_utility = 0;
    
    for (int i = 0; i < query_num; i++) {
        mypoint rpoint = randomPoint(range, r);
        
        double pLow[] = {rpoint.x - r, rpoint.y - r};
        double pHigh[] = {rpoint.x + r, rpoint.y + r};
        Region region(pLow, pHigh, 2);
        
        IdVisitor vis;
        tree->intersectsWithQuery(region, vis);
        
        std::vector<uint64_t> results = vis.GetResults();
        double this_utility = utility(countWithin(results, rpoint, uid_map, dataArray, r), param);
        
        if (this_utility > 0) {
            total_utility += this_utility;
            valid_num++;
        }
    }
    
    return total_utility / query_num;
}




int main(int argc, const char * argv[]) {
    
    assert(argc == 8);
    cout << "Random Points-based Utility Computations." << endl;
    
    string inputFileName(argv[1]);      // sample file
    double r    = (float) strtod(argv[2], (char **) NULL);

    double xmin = (float) strtod(argv[3], (char **) NULL);
    double xmax = (float) strtod(argv[4], (char **) NULL);
    double ymin = (float) strtod(argv[5], (char **) NULL);
    double ymax = (float) strtod(argv[6], (char **) NULL);

    double param = (float) strtod(argv[7], (char **) NULL);
    
    vector<double> range;
    range.push_back(xmin);
    range.push_back(xmax);
    range.push_back(ymin);
    range.push_back(ymax);
    
    cout << "Input file: " << inputFileName << endl;
    
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    
    // read all data.
    vector<DataPoint> dataArray;
    
    DataPoint* point;
    while ((point = fileSource.readNext())) {
        dataArray.push_back(*point);
        delete point;
    }
    
    
    // output
    double utility = compute_average_utility(dataArray, range, r, param);
    
    cout << "Average Utility: " << utility << endl << endl;
    
    return 0;
}





