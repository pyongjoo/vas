//
//  SingleStage.h
//  pas
//
//  Created by Yongjoo Park on 11/29/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#ifndef __pas__Slow2SingleStage__
#define __pas__Slow2SingleStage__

#include "DataSource.h"
#include "SingleStage.h"
#include <iostream>
#include <unordered_map>
#include <vector>



// Main class
class Slow2PASSampler {
    
protected:
    
    // Priority Queue (used to maintain the data points in the order of
    // responsibility).
    // rsp_S (x) = w + \sum_{s_i \in S, s_i \ne x} w_i w circleIntersect(r, \|s_i - x\|)
    CustomQueue queue;
    
    
protected:
    
    float r;            // radius of local focus
    float alpha;        // weight term
    float beta;         // weight term
    int k;              // sample size
    

    double resp_reduction(const std::vector<RespPoint>& points, const DataPoint& newPoint, const int idx);
    
public:
    
    /**
     * @param alpha weights for individual items (set 0 for defaults)
     * @param beta weights for pairwise interaction (set 1 for defaults)
     */
    Slow2PASSampler(float r, float alpha, float beta, int k);
    
    ~Slow2PASSampler();

    // construct well-distributed sample set using stratified sampling
    virtual void warm(DataSource& source);
    
    // Check for valid replacement.
    virtual void check(const DataPoint& point);
    
    virtual void Expand(const DataPoint& point);
    
    virtual void Replace(const DataPoint& point);
    
    virtual std::vector<DataPoint> getSampled();

};



void Slow2SingleStageSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup = false);

#endif /* defined(__pas__SingleStage__) */
