//
//  SingleStage.h
//  pas
//
//  Created by Yongjoo Park on 11/29/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#ifndef __pas__SingleStage__
#define __pas__SingleStage__

#include "DataSource.h"
#include <iostream>
#include <spatialindex/SpatialIndex.h>
#include <spatialindex/Point.h>
#include <spatialindex/capi/IdVisitor.h>
#include <unordered_map>
#include <vector>


using namespace SpatialIndex;


// data structure to be used in a queue
class RespPoint {
    
public:
    
    DataPoint point;
    float resp;         // responsibility in a sample
    int qid;            // index in queue
    
public:
    
    RespPoint();        // this should not be called
    RespPoint(DataPoint point, float resp);
};



// This is min-heap whose elements are RespPoint.
class CustomQueue {
    
public:
    
    std::vector<RespPoint> _dataArray;

    std::unordered_map<data_id_t,int> uid_map;
    
    
    // Convenience Methods
    
    int getLeftChildIndex(const int dataIndex) const;
    
    int getRightChildIndex(const int dataIndex) const;
    
    int getParentIndex(const int dataIndex) const;
    
    float getResp(const int dataIndex) const;
    
    
public:
    
//    CustomQueue(float alpha, float beta);

    RespPoint& get(data_id_t uid);
    
    // set 'qid' appropriately
    void insert(RespPoint& point);
    
    RespPoint pop();
    
    // should update the queue_index of elements accordingly
    void raiseIfNeeded(const int dataIndex);
    
    // should update the queue_index of elements accordingly
    void lowerIfNeeded(const int dataIndex);

    void updateQid(const int dataIndex);
    
//    void print() const;
    
};


// intersection between the two circles when the radiuses of the two circles
// are 'r', and the distance between the two circles is 'd'.
//
// approximate it as a linear function.
// overlap = (1) 1 - d/2r if 0 <= d <= 2r
//           (2) 0 if d > 2r
float circleIntersect(const float r, const float d);

float circleIntersect(const DataPoint& p1, const DataPoint& p2, const float r);




// Main class
class PASSampler {
    
protected:
    
    // R-tree
    IStorageManager *memfile;
    
    ISpatialIndex* tree;
    
    
    // Priority Queue (used to maintain the data points in the order of
    // responsibility).
    // rsp_S (x) = w + \sum_{s_i \in S, s_i \ne x} w_i w circleIntersect(r, \|s_i - x\|)
    CustomQueue queue;
    
    
    // Lookup Table of DataPoints
    // This is the primary place that stores data.
    std::unordered_map<data_id_t, RespPoint> sampleBase;
    
    
protected:
    
    float r;            // radius of local focus
    float alpha;        // weight term
    float beta;         // weight term
    int k;              // sample size
    
    
public:
    
    /**
     * @param alpha weights for individual items (set 0 for defaults)
     * @param beta weights for pairwise interaction (set 1 for defaults)
     */
    PASSampler(float r, float alpha, float beta, int k);
    
    ~PASSampler();

    // construct well-distributed sample set using stratified sampling
    virtual void warm(DataSource& source);
    
    // Check for valid replacement.
    virtual void check(const DataPoint& point);
    
    virtual void Expand(const DataPoint& point);
    
    virtual void Shrink(const DataPoint& added_point);
    
    virtual std::vector<DataPoint> getSampled();

public:
    void pseudoCheck(const DataPoint& point);
};


float recommendedR(const float xmin, const float xmax, const float ymin, const float ymax, const int k);

void writeSampledTo(const std::string outputFileName, const std::vector<DataPoint>& sampled);

void SingleStageSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup = false);

void GradualSampleWrapper(const std::string inputFileName,
        const std::string outputFileName,
        const float alpha, const float beta, const int k, const std::vector<float>& range,
        const int sweep_num, const float sample_prob, const bool warmup = false);


#endif /* defined(__pas__SingleStage__) */
