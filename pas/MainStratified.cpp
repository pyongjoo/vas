#include <cassert>
#include <iostream>
#include <string>
#include "RandomSampling.h"


using namespace std;


// Usage:
// ./stratified inputFileName outputFileName \
//          sampleSize strataNum xmin xmax ymin ymax
// where strataNum is the total strata count (horizontal count * vertical count)


int main(int argc, const char * argv[])
{
    assert(argc == 9);

    string inputFileName(argv[1]);
    string outputFileName(argv[2]);
    int sample_size  = (int) strtol(argv[3], (char **) NULL, 10);
    int strata_num   = (int) strtol(argv[4], (char **) NULL, 10);

    float xmin = (float) strtod(argv[5], (char **) NULL);
    float xmax = (float) strtod(argv[6], (char **) NULL);
    float ymin = (float) strtod(argv[7], (char **) NULL);
    float ymax = (float) strtod(argv[8], (char **) NULL);

    vector<float> range;
    range.push_back(xmin);
    range.push_back(xmax);
    range.push_back(ymin);
    range.push_back(ymax);

    StratifiedSamplerWrapper(inputFileName, outputFileName, sample_size, strata_num, range);

    return 0;
}

