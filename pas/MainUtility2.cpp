//
//  MainUtility.cpp
//  pas
//
//  Created by Yongjoo Park on 12/3/14.
//  Copyright (c) 2014 Yongjoo Park. All rights reserved.
//

#include <algorithm>
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctgmath>
#include <spatialindex/SpatialIndex.h>
#include <spatialindex/Point.h>
#include <spatialindex/capi/IdVisitor.h>
#include <unordered_map>
#include "DataSource.h"
#include "SingleStage.h"


using namespace std;
using namespace SpatialIndex;


double utility(const vector<uint64_t>& results,
               const DataPoint& p,
               const double r,
               unordered_map<long, long>& uid_map,
               const vector<DataPoint>& dataArray,
               double param)
{

    double loss_sum = 0;

    for (vector<uint64_t>::const_iterator it = results.begin(); it != results.end(); it++) {
        long uid = *it;
        int index = uid_map[uid];
        const DataPoint& p1 = dataArray[index];
        loss_sum += circleIntersect(p1, p, r);
    }

    //return 1.0 / (1 + param*loss_sum);
    return param*loss_sum;
}


double compute_average_utility(vector<DataPoint>& dataArray,
        const vector<double>& range, const double r, const double param)
{
    
    // prepare r-tree
    IStorageManager* memfile = StorageManager::createNewMemoryStorageManager();
    
    id_type indexIdentifier;
    int indexCap = 10;
    int leafCap = 10;
    int dim = 2;
    ISpatialIndex* tree = RTree::createNewRTree(*memfile, 0.7,
                                 indexCap, leafCap, dim,
                                 RTree::RV_RSTAR, indexIdentifier);
    
    // insert points
    for (vector<DataPoint>::const_iterator it = dataArray.begin();
         it != dataArray.end(); it++) {
        
        const DataPoint& point = *it;
        
        double coordi[] = {point.x, point.y};
        Point p(coordi, 2);
        tree->insertData(0, NULL, p, point.uid);
    }
    
    
    // UID to index map
    unordered_map<long, long> uid_map;
    long index = 0;
    for (vector<DataPoint>::const_iterator it = dataArray.begin();
         it != dataArray.end(); it++) {
        
        const DataPoint& point = *it;
        uid_map.insert(make_pair(point.uid, index));
        index++;
    }
    
    
    // query step
    int query_num = 10000;
    double total_utility = 0;

    //random_shuffle(dataArray.begin(), dataArray.end());

    for (int i = 0; i < query_num; i++) {
        DataPoint p = dataArray[i];
        
        double pLow[] = {p.x - 3*r, p.y - 3*r};
        double pHigh[] = {p.x + 3*r, p.y + 3*r};
        Region region(pLow, pHigh, 2);
        
        IdVisitor vis;
        tree->intersectsWithQuery(region, vis);
        
        std::vector<uint64_t> results = vis.GetResults();
        double this_utility = utility(results, p, r, uid_map, dataArray, param);
        
        total_utility += this_utility;
    }
    
    return total_utility / query_num;
}




int main(int argc, const char * argv[]) {
    
    assert(argc == 8);
    cout << "Sampled Points-based Utility Computations." << endl;
    
    string inputFileName(argv[1]);      // sample file
    double r    = (float) strtod(argv[2], (char **) NULL);

    double xmin = (float) strtod(argv[3], (char **) NULL);
    double xmax = (float) strtod(argv[4], (char **) NULL);
    double ymin = (float) strtod(argv[5], (char **) NULL);
    double ymax = (float) strtod(argv[6], (char **) NULL);

    double param = (float) strtod(argv[7], (char **) NULL);
    
    vector<double> range;
    range.push_back(xmin);
    range.push_back(xmax);
    range.push_back(ymin);
    range.push_back(ymax);
    
    cout << "Input file: " << inputFileName << endl;
    
    FileDataSource fileSource(inputFileName, xmin, xmax, ymin, ymax);
    
    // read all data.
    vector<DataPoint> dataArray;
    
    DataPoint* point;
    while ((point = fileSource.readNext())) {
        dataArray.push_back(*point);
        delete point;
    }
    
    
    // output
    double utility = compute_average_utility(dataArray, range, r, param);
    
    cout << "Average Utility: " << utility << endl << endl;
    
    return 0;
}

