import numpy as np
import matplotlib.pyplot as plt

def visualize(filename):

    xx = []
    yy = []
    cc = []

    for line in open(filename):
        x, y, z, w, d = line.rstrip('\n').split(',')
        xx.append(float(x))
        yy.append(float(y))
        cc.append(float(z))

    cc = map(lambda a: max(1, a), cc)
    cc = np.array(cc, dtype=float)
    cc = np.log2(cc)

    #vmin = np.percentile(cc, 10)
    #vmax = np.percentile(cc, 90)
    vmin = 4.38729346955
    vmax = 13.3149539245

    sc = plt.scatter(yy, xx, s=6, c=cc, cmap='gist_earth',
            vmin=vmin, vmax=vmax,
            edgecolors='none', alpha=0.5)
    plt.grid(b=True, which='both')
    plt.colorbar(sc)
    plt.xlabel('longitude')
    plt.ylabel('latitude')

    plt.show()


if __name__ == "__main__":
    filename = '/z/pyongjoo_data/pas/sampled/icde_2016/open_stratified_1M.csv'
    visualize(filename)


