import numpy as np
import matplotlib.pyplot as plt

def visualize(filename):
    mina = -50.0
    maxa = 1000.0

    xx = []
    yy = []
    cc = []

    for line in open(filename):
        x, y, z, w, l = line.rstrip('\n').split(',')

        x = float(x)
        y = float(y)
        z = float(z)

        if not (90 < y < 135):
            continue

        xx.append(x)
        yy.append(y)

        if z < mina: z = mina
        if z > maxa: z = maxa
        cc.append(z)

    cc = map(lambda a: max(1, a), cc)
    cc = np.array(cc, dtype=float)

    #vmin = np.percentile(cc, 10)
    #vmax = np.percentile(cc, 90)
    vmin = 4.38729346955
    vmax = 13.3149539245

    sc = plt.scatter(yy, xx, s=5, c=cc, cmap='gist_earth', vmin=mina,
            vmax=maxa+300, edgecolors='none', alpha=0.7)
    plt.grid(b=True, which='both')
    plt.colorbar(sc)
    plt.xlabel('longitude')
    plt.ylabel('latitude')

    plt.xlim([116,118])
    plt.ylim([38.5,40.5])

    plt.show()


if __name__ == "__main__":
    filename = '/z/pyongjoo_data/pas/sampled/icde_2016/geolife_stratified_100K.csv'
    filename = '/z/pyongjoo_data/pas/sampled/geolife_gradual_100K.csv0'
    #filename = '/z/pyongjoo_data/pas/sampled/icde_2016/geolife_stratified_100K.csv'
    filename = '/z/pyongjoo_data/pas/sampled/icde_2016/geolife_single_100K.csv'
    visualize(filename)


